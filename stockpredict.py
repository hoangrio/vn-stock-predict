from os import path
import os
from config import MODEL_PATH, DATA_SOURCE, DOWNLOAD_DAYS_RANGE, DATE_FORMAT, DATA_PATH, LOG_PATH, SUPPORTED_PRICE_TYPE, INPUT_RANGE_DAYS, SUPPORTED_PRICE_TYPE_SWAP
from vnquant.data import DataLoader
from datetime import datetime, timedelta
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
from pandas.tseries.offsets import BDay
import logging
import matplotlib.pyplot as plt
logger = logging.getLogger(__name__)


class StockPredict():
    id = None
    is_trained = False
    is_traning = False
    __test_mode = False
    __swap_low_high = False

    def __init__(self, id, price_type = 'close', test_mode=False, swap_low_high=True) -> None:
        if id is None or id == "":
            raise ValueError("Stock id cloud not be None or empty")
        self.id = id
        self.__test_mode = test_mode
        self.__swap_low_high = swap_low_high
        if price_type not in SUPPORTED_PRICE_TYPE:
            raise ValueError("Price type must be %s", SUPPORTED_PRICE_TYPE)
        self.__price_type = price_type
        ## Setup Log for stockID
        fh = logging.FileHandler(LOG_PATH + '%s.log' % self.id)
        fh.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s: %(filename)s -> %(funcName)s:%(lineno)d: %(message)s'))
        fh.setLevel(logging.DEBUG)
        logger.addHandler(fh)
        logger.setLevel(logging.DEBUG)
        ## End setup for stockID
        self.data_day = datetime.now().replace(
            hour=0,  minute=0,  second=0)
        if datetime.now().hour < 18:
            self.data_day = self.__subtract_date_data(self.data_day, 1)
        self.__data_path = self.__get_data_path(self.data_day)
        self.__model_path = self.__get_model_path(self.data_day)
        if path.exists(self.__model_path):
                self.is_trained = True

    def __subtract_date_data(self, data_date, addition_subtract=0):
        return data_date - BDay(addition_subtract)

    def __get_data_path(self, data_date):
        return DATA_PATH + self.id + \
            \
            "-" + data_date.strftime(DATE_FORMAT) + ".csv"

    def __get_model_path(self, data_date):
        return MODEL_PATH + self.id + '-' + ('testmode-' if self.__test_mode else '') + self.__price_type + '-' + data_date.strftime(DATE_FORMAT) + ".keras"

    def __build_model(self, x_train):
         # Xay dung model LSTM
        self.model = Sequential()
        self.model.add(
            LSTM(units=50, return_sequences=True, input_shape=(x_train.shape[1], 1)))
        self.model.add(Dropout(0.2))
        self.model.add(LSTM(units=50, return_sequences=True))
        self.model.add(Dropout(0.2))
        self.model.add(LSTM(units=50, return_sequences=True))
        self.model.add(Dropout(0.2))
        self.model.add(LSTM(units=50))
        self.model.add(Dropout(0.2))
        self.model.add(Dense(units=1))
        self.model.compile(optimizer='adam', loss='mean_squared_error')

    def __get_price_index(self):
        if self.__swap_low_high:
            return  SUPPORTED_PRICE_TYPE_SWAP.index(self.__price_type) + 2
        return SUPPORTED_PRICE_TYPE.index(self.__price_type) + 2

    def __train(self):
        if (self.is_traning):
            return False
        if self.__test_mode:
            logger.debug("Runing train model of %s for %s price in test mode with %s session", self.id, self.__price_type, self.__test_session_count)
        self.is_traning = True
        try:
            # Start Tranning model
            # Start download data of 365 days
            if not path.exists(self.__data_path):
                today = self.data_day
                last_year = today - timedelta(days=DOWNLOAD_DAYS_RANGE)
                loader = DataLoader(symbols=self.id,
                                    start=last_year.strftime(DATE_FORMAT),
                                    end=today.strftime(DATE_FORMAT),
                                    minimal=True,
                                    data_source=DATA_SOURCE)
                logger.info("Start download data of %s", self.id)
                self.stock_data = loader.download().sort_values(by='date', ascending=True)
                self.stock_data.to_csv(self.__data_path)
                logger.info("Finished download data of %s", self.id)
                previous_day = self.__subtract_date_data(self.data_day, 1)
                if path.exists(self.__get_data_path(previous_day)):
                    logger.info("Deleting old data file at %s of %s",previous_day.strftime(DATE_FORMAT), self.id)
                    os.unlink(self.__get_data_path(previous_day))
            self.stock_data = pd.read_csv(self.__data_path)
            self.stock_data = self.stock_data.drop([0, 1])
            self.stock_data.fillna(value=-999, inplace=True)
            price_type_index = self.__get_price_index()
            if self.__test_mode:
                self.training_set = self.stock_data.iloc[:len(self.stock_data) - self.__test_session_count, price_type_index:price_type_index + 1].values
            else:
                self.training_set = self.stock_data.iloc[:, price_type_index:price_type_index + 1].values

            # Thuc hien scale du lieu gia ve khoang 0,1
            self.sc = MinMaxScaler(feature_range=(0, 1))
            training_set_scaled = self.sc.fit_transform(self.training_set)

            # Tao du lieu train, X = INPUT_RANGE_DAYS time steps, Y =  1 time step
            x_train = []
            y_train = []
            no_of_sample = len(self.training_set)

            for i in range(INPUT_RANGE_DAYS, no_of_sample):
                x_train.append(training_set_scaled[i-INPUT_RANGE_DAYS:i, 0])
                y_train.append(training_set_scaled[i, 0])

            x_train, y_train = np.array(x_train), np.array(y_train)

            x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))
            self.__build_model(x_train)
            if self.is_trained:
                self.model.load_weights(self.__model_path)
            else:
                logger.info("Start traning model for %s at %s price", self.id, self.__price_type)
                self.model.fit(x_train, y_train, epochs=100, batch_size=32)
                self.model.save(self.__model_path)
                logger.info("Finish traning model for %s at %s price", self.id, self.__price_type)
                previous_day = self.__subtract_date_data(self.data_day, 1)
                if path.exists(self.__get_model_path(previous_day)):
                    logger.info("Deleting old data file at %s of %s",previous_day.strftime(DATE_FORMAT), self.id)
                    os.unlink(self.__get_model_path(previous_day))
            # End train model
        except Exception as e:
            logger.error("Got exception when traning %s: %s", self.id, e)
            if path.exists(self.__data_path):
                os.unlink(self.__data_path)
            raise e
        self.is_traning = False

    def predict(self, session_count=5):
        """
        Predict stock price
        @params session_count Days to predict
        """
        self.__train()
        logger.info("Start predict %s price for stockID %s", self.__price_type, self.id)
        # Doi cho column low va high do data loader bi sai
        price_type = self.__price_type
        if self.__swap_low_high:
            if self.__price_type == 'high':
                price_type = 'low'
            elif self.__price_type == 'low':
                price_type = 'high'
        # Tien hanh du doan
        total_size = len(self.stock_data[price_type])
        dataset_test = self.stock_data[price_type][total_size - INPUT_RANGE_DAYS: total_size].to_numpy()
        inputs = dataset_test
        inputs = inputs.reshape(-1, 1)
        inputs = self.sc.transform(inputs)

        date = self.data_day
        date = date + BDay(1)
        predict_price = []
        date_label = []
        date_index = []

        i = 0
        while i<session_count:
            x_test = []
            no_of_sample = len(dataset_test)

            # Lay du lieu cuoi cung
            x_test.append(inputs[no_of_sample - INPUT_RANGE_DAYS:no_of_sample, 0])
            x_test = np.array(x_test)
            x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))

            # Du doan gia
            predicted_stock_price = self.model.predict(x_test)

            # chuyen gia tu khoang (0,1) thanh gia that
            predicted_stock_price = self.sc.inverse_transform(predicted_stock_price)

            # Them ngay hien tai vao
            dataset_test = np.append(dataset_test, predicted_stock_price[0], axis=0)
            inputs = dataset_test
            inputs = inputs.reshape(-1, 1)
            inputs = self.sc.transform(inputs)
            predict_price.append(predicted_stock_price[0][0])
            date_label.append(date.strftime('%d/%m'))
            date_index.append(i)
            logger.info("Predict %s price of %s in %s is: %s",self.__price_type, self.id,

                  date.strftime(DATE_FORMAT), predicted_stock_price[0][0])
            date = date + BDay(1)
            i = i + 1
        plt.plot(predict_price, color = 'blue', label = 'Predicted %s %s Stock Price' % (self.id, self.__price_type))
        plt.title('%s %s Stock Price Prediction' % (self.id, self.__price_type))
        plt.xlabel('Date')
        plt.xticks(date_index, date_label)
        plt.ylabel('%s %s Stock Price' % (self.id, self.__price_type))
        plt.legend()
        plt.show()

        logger.info("Finish predict %s price for stockID %s",self.__price_type, self.id)

    def test(self, session_count=5):
        """
        WIP: Test predict result. Dont run it now because it error
        """
        self.__test_session_count = session_count
        self.__train()
        logger.info("Start test predict %s price for stockID %s", self.__price_type, self.id)
        # Doi cho column low va high do data loader bi sai
        price_type = self.__price_type
        if self.__swap_low_high:
            if self.__price_type == 'high':
                price_type = 'low'
            elif self.__price_type == 'low':
                price_type = 'high'
        # Tien hanh test
        total_size = len(self.stock_data[price_type]) - session_count
        dataset_test = self.stock_data[price_type][total_size - INPUT_RANGE_DAYS: total_size].to_numpy()
        inputs = dataset_test
        inputs = inputs.reshape(-1, 1)
        inputs = self.sc.transform(inputs)

        date = self.__subtract_date_data(self.data_day, session_count - 1)
        price_type_index = SUPPORTED_PRICE_TYPE.index(self.__price_type) + 1
        real_stock_price = self.stock_data.iloc[len(self.stock_data) - session_count:len(self.stock_data), price_type_index:price_type_index+1].values
        real_price = []
        predict_price = []
        date_label = []
        date_index = []
        predict_diff = []

        i = 0
        while i<session_count:
            x_test = []
            no_of_sample = len(dataset_test)

            # Lay du lieu cuoi cung
            x_test.append(inputs[no_of_sample - INPUT_RANGE_DAYS:no_of_sample, 0])
            x_test = np.array(x_test)
            x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))

            # Du doan gia
            predicted_stock_price = self.model.predict(x_test)

            # chuyen gia tu khoang (0,1) thanh gia that
            predicted_stock_price = self.sc.inverse_transform(predicted_stock_price)

            # Them ngay hien tai vao
            dataset_test = np.append(dataset_test, real_stock_price[i], axis=0)
            inputs = dataset_test
            inputs = inputs.reshape(-1, 1)
            inputs = self.sc.transform(inputs)
            predict_price.append(predicted_stock_price[0][0])
            real_price.append(float(real_stock_price[i][0]))
            predict_diff.append(float(real_stock_price[i][0]) - predicted_stock_price[0][0])
            date_label.append(date.strftime('%d/%m'))
            date_index.append(i)
            logger.info("Predict %s price of %s in %s is: %s",self.__price_type, self.id,

                  date.strftime(DATE_FORMAT), predicted_stock_price[0][0])
            logger.info("Real %s price of %s in %s is: %s",self.__price_type, self.id,

                  date.strftime(DATE_FORMAT), real_stock_price[i][0])
            date = date + BDay(1)
            i = i + 1
        logger.info('Average %s price of %s predict diff: %f',self.__price_type, self.id, np.average(predict_diff))
        plt.plot(real_price, color = 'red', label = 'Real %s %s Stock Price' % (self.id, self.__price_type))
        plt.plot(predict_price, color = 'blue', label = 'Predicted %s %s Stock Price' % (self.id, self.__price_type))
        plt.title('%s %s Stock Price Prediction' % (self.id, self.__price_type))
        plt.xlabel('Date')
        plt.xticks(date_index, date_label)
        plt.ylabel('%s %s Stock Price' % (self.id, self.__price_type))
        plt.legend()
        plt.show()
        os.unlink(self.__model_path)
        logger.info("Finish test predict %s price for stockID %s", self.__price_type, self.id)