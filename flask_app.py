from flask import Flask
from priceloader import loadData

app = Flask(__name__)

@app.route("/")
def home():
    return "Hello world from flask" + loadData("VCB")

if __name__ == "__main__":
    app.run(debug=True)
