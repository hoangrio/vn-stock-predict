import sys
from stockpredict import StockPredict
from config import SUPPORTED_PRICE_TYPE

stock = StockPredict(sys.argv[1] if len(sys.argv) > 1 else "HVN", sys.argv[3] if len(sys.argv) > 3 else SUPPORTED_PRICE_TYPE[3], False, bool(sys.argv[4]) if len(sys.argv) > 4 else False)
stock.predict(int(sys.argv[2]) if len(sys.argv) > 2 else 5)
