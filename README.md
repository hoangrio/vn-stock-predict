# VN Stock Predict

Use ML to Predict Stock price with Python

## Configuration
* Open config.py and change configuration parametter:
    * `DOWNLOAD_DAYS_RANGE` is number of days to download data. Default is 3 year
    * `LOG_PATH` is log path to save predict log. Each stock id will be log to file with name [stockid].log
## Installation

* Install python 3.12 in your system (**Important**)
* Run `python -m venv venv` to create virtual env system
* Run `source venv/Script/active` to activate virtual env
* Run `pip install -r requirements.txt` to install dependencies
* Init and update git submodule with command `git submodule init && git submodule update` **(Warn: This command will access github to download git submodule)**
* Install vnquant for dataloader `cd vnquant && python setup.py install`

## Run
_Only support stockID on HNX or HSX(HOSE)_
* `python . "[stockid]" [day to predict] "[price type (high,low,open,close,adjust)] [Swap low high price]"` `[Swap low high price]` mean low and high price column is incorrect and must be swap. Please check in data folder. It happened on some stock id.
* Example `python . "HVN" 30 "high" True` to predict next 30 session high price of HVN

## Test
_Only support stockID on HNX or HSX(HOSE)_
* `python test.py "[stockid]" [day to test] "[price type (high,low,open,close,adjust)] [Swap low high price]"` `[Swap low high price]` mean low and high price column is incorrect and must be swap. Please check in data folder. It happened on some stock id.
* Example `python test.py "HVN" 30 "high" True` to test previos 30 session high price of HVN