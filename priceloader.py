def loadData(stock_id="VIC"):
    from vnquant.data import DataLoader

    loader = DataLoader(symbols=stock_id,
            start="2020-02-01",
            end="2021-03-01",
            minimal=False,
            data_source="cafe")

    data_vic = loader.download()
    data_vic.tail()
    print(data_vic)
    return data_vic
